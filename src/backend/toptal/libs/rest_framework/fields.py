import arrow

from django.utils import html
from rest_framework.fields import CharField, DateTimeField

from google.appengine.ext import ndb


class SafeCharField(CharField):

    def to_representation(self, value):
        safe_value = html.escape(value)
        return super(SafeCharField, self).to_representation(safe_value)


class UrlsafeKeyPropertyField(CharField):

    def to_internal_value(self, value):  # pragma: no cover
        if value:
            return ndb.Key(urlsafe=value)
        else:
            return None

    def to_representation(self, value):
        return value.urlsafe()


class UTCDateTimeField(DateTimeField):

    def to_internal_value(self, value):
        result = super(UTCDateTimeField, self).to_internal_value(value)
        return arrow.get(result).to('UTC').datetime.replace(tzinfo=None)

    def to_representation(self, value):
        if not value.microsecond:
            value = value.replace(microsecond=1)
        return super(UTCDateTimeField, self).to_representation(value)
