from rest_framework.response import Response
from rest_framework.compat import OrderedDict
from rest_framework.pagination import BasePagination, _positive_int
from rest_framework.utils.urls import replace_query_param

from google.appengine.datastore.datastore_query import Cursor


class NDBCursorPagination(BasePagination):
    """
    A limit/offset based style. For example:

    http://api.example.org/accounts/?limit=100
    http://api.example.org/accounts/?offset=400&limit=100
    """
    default_limit = 20
    max_limit = 100
    count_query_param = 'count'
    cursor_query_param = 'cursor'

    def get_count(self, request):
        try:
            return _positive_int(
                request.query_params[self.count_query_param],
                cutoff=self.max_limit
            )
        except (KeyError, ValueError):
            return self.default_limit

    def get_cursor_options(self, request):
        cursor = request.query_params.get(self.cursor_query_param)
        if cursor:  # pragma: no cover
            return {'start_cursor': Cursor(urlsafe=cursor)}
        return {}

    def paginate_queryset(self, queryset, request, view=None):
        self.request = request
        self.count = self.get_count(request)
        if self.count is None:  # pragma: no cover
            return None

        if queryset._maybe_multi_query():
            queryset = queryset.order(view.model_class.key)

        options = self.get_cursor_options(request)
        results, next_cursor, has_more = queryset.fetch_page(
            self.count, **options)
        self.next_cursor = next_cursor
        self.has_next = has_more
        return results

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('data', data),
            ('pagination', OrderedDict([
                ('next_url', self.get_next_link()),
            ]))
        ]))

    def get_next_link(self):
        if not self.has_next:
            return None
        url = self.request.get_full_path()
        return replace_query_param(
            url, self.cursor_query_param, self.next_cursor.urlsafe())
