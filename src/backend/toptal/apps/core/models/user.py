from google.appengine.ext import ndb

from base import BaseModel


class User(BaseModel):
    """
    User has key is username
    """

    (
        USER_STATUS_NORMAL,
        USER_STATUS_DELETED,
    ) = xrange(2)

    USER_STATUS_CHOICES = (
        (USER_STATUS_NORMAL, 'Status normal'),
        (USER_STATUS_DELETED, 'Status deleted'),
    )

    (
        USER_ROLE_TYPE_NORMAL,
        USER_ROLE_TYPE_USER_MANAGER,
        USER_ROLE_TYPE_ADMIN,
    ) = xrange(3)

    USER_ROLE_TYPE_CHOICES = (
        (USER_ROLE_TYPE_NORMAL, 'Role type normal'),
        (USER_ROLE_TYPE_USER_MANAGER, 'Role type user manager'),
        (USER_ROLE_TYPE_ADMIN, 'Role type admin')
    )

    name = ndb.StringProperty()
    password = ndb.StringProperty()
    role_type = ndb.IntegerProperty(default=USER_ROLE_TYPE_NORMAL)
    status = ndb.IntegerProperty(default=USER_STATUS_NORMAL)

    @property
    def username(self):
        return self.key.id()
