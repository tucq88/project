from google.appengine.ext import ndb

from base import BaseModel


class Report(BaseModel):

    (
        REPORT_STATUS_NORMAL,
        REPORT_STATUS_DELETED,
    ) = xrange(2)

    REPORT_STATUS_CHOICES = (
        (REPORT_STATUS_NORMAL, 'Status normal'),
        (REPORT_STATUS_DELETED, 'Status deleted'),
    )
    user_key = ndb.KeyProperty(kind='User')
    total_distance = ndb.FloatProperty(default=0)
    total_duration = ndb.IntegerProperty(default=0)
    start_at = ndb.DateProperty()
    end_at = ndb.DateProperty()
    status = ndb.IntegerProperty(default=REPORT_STATUS_NORMAL)

    @classmethod
    def report_key(self, user_key, start_at, end_at):
        return ndb.Key('Report', '%s:%s-%s' % (
            user_key.id(),
            start_at.strftime('%Y%m%d'),
            end_at.strftime('%Y%m%d')))

    @property
    def average_speed(self):
        return self.total_distance / max(self.total_duration, 1)

    @ndb.tasklet
    def user_async(self, user, request=None):
        result = yield self.user_key.get_async()
        raise ndb.Return(result)
