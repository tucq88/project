from google.appengine.ext import ndb

from toptal.libs import utils

from base import BaseModel


class Entry(BaseModel):

    (
        ENTRY_STATUS_NORMAL,
        ENTRY_STATUS_DELETED,
    ) = xrange(2)

    ENTRY_STATUS_CHOICES = (
        (ENTRY_STATUS_NORMAL, 'Status normal'),
        (ENTRY_STATUS_DELETED, 'Status deleted'),
    )

    user_key = ndb.KeyProperty(kind='User')
    distance = ndb.FloatProperty()
    start_at = ndb.DateTimeProperty()
    end_at = ndb.DateTimeProperty()
    status = ndb.IntegerProperty(default=ENTRY_STATUS_NORMAL)

    @property
    def duration(self):
        return int((self.end_at - self.start_at).total_seconds())

    @property
    def average_speed(self):
        return self.distance / max(self.duration, 1)

    @utils.lazy
    def start_week_at(self):
        return utils.start_week_of(self.start_at)

    @utils.lazy
    def end_week_at(self):
        return utils.end_week_of(self.end_at)

    @utils.lazy
    def report_key(self):
        from report import Report
        return Report.report_key(
            self.user_key, self.start_week_at, self.end_week_at)

    @ndb.tasklet
    def user_async(self, user, request=None):
        result = yield self.user_key.get_async()
        raise ndb.Return(result)
