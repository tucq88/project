from google.appengine.ext import ndb

from toptal.libs import utils
from toptal.exceptions import PermissionDeniedError
from toptal.apps.core.models import User, Entry, Report


class EntryUpdateService(object):

    def __init__(self, user, entry, **data):
        self.user = user
        self.entry = entry
        self.data = data

    def check_permission(self):
        is_entry_owner = self.entry.user_key == self.user.key
        is_admin = self.user.role_type == User.USER_ROLE_TYPE_ADMIN
        if not is_entry_owner and not is_admin:
            raise PermissionDeniedError(
                'user does not have permission to edit entry')

    def is_change_week(self, old_data, new_data):
        if 'start_at' not in new_data and 'end_at' not in new_data:
            return False
        elif 'start_at' in new_data:
            old_start_week = utils.start_week_of(old_data['start_at'])
            new_start_week = utils.start_week_of(new_data['start_at'])
            return old_start_week != new_start_week
        else:
            old_start_week = utils.start_week_of(old_data['end_at'])
            new_start_week = utils.start_week_of(new_data['end_at'])
            return old_start_week != new_start_week

    @ndb.tasklet
    def create_new_report(self, entry):
        report = yield entry.report_key.get_async()
        if not report:
            report = yield Report.create_async(
                key=entry.report_key,
                start_at=entry.start_week_at,
                end_at=entry.end_week_at,
                total_distance=entry.distance,
                total_duration=entry.duration)
        else:
            yield report.update_async(
                total_distance=report.total_distance + entry.distance,
                total_duration=report.total_duration + entry.total_duration)
        raise ndb.Return(report)

    @ndb.tasklet
    def decrease_report(self, old_entry):
        report = yield old_entry.report_key.get_async()
        yield report.update_async(
            total_distance=report.total_distance - old_entry.distance,
            total_duration=report.total_duration - old_entry.duration)

    def is_need_change_report(self, old_data, new_data):
        is_change_distance = 'distance' in new_data
        is_change_duration = 'start_at' in new_data or 'end_at' in new_data
        return is_change_distance or is_change_duration

    @ndb.tasklet
    def update_old_report(self, old_entry, entry):
        report = yield old_entry.report_key.get_async()
        new_distance = (
            report.total_distance - old_entry.distance + entry.distance)
        new_duration = (
            report.total_duration - old_entry.duration + entry.duration)
        yield report.update_async(
            total_distance=new_distance, total_duration=new_duration)
        raise ndb.Return(report)

    @ndb.tasklet
    def update_report(self, old_entry, entry, old_data, new_data):
        report = None
        if self.is_change_week(old_data, new_data):
            report, _ = yield (
                self.create_new_report(entry),
                self.decrease_report(old_entry.report_key))
        elif self.is_need_change_report(old_data, new_data):
            report = yield self.update_old_report(old_entry, entry)
        raise ndb.Return(report)

    @ndb.transactional_tasklet(xg=True, retries=10)
    def update_entry_and_report(self):
        entry = self.entry.key.get()
        old_entry = Entry(
            user_key=entry.user_key, distance=entry.distance,
            start_at=entry.start_at, end_at=entry.end_at)
        old_data, new_data = entry.build_update(**self.data)
        _, report = yield (
            entry.put_async(),
            self.update_report(old_entry, entry, old_data, new_data))
        raise ndb.Return(entry, report)

    @ndb.tasklet
    def execute_async(self):
        self.check_permission()
        entry, report = yield self.update_entry_and_report()
        raise ndb.Return(entry, report)

    def execute(self):
        return self.execute_async().get_result()
