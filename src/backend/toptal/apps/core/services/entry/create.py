from google.appengine.ext import ndb

from toptal.apps.core.models import Entry, Report


class EntryCreateService(object):

    def __init__(self, user, **data):
        self.user = user
        self.data = data

    @ndb.transactional_tasklet(retries=10)
    def update_or_create_report(self, entry):
        report = yield entry.report_key.get_async()
        if not report:
            report = yield Report.create_async(
                key=entry.report_key,
                user_key=self.user.key,
                start_at=entry.start_week_at,
                end_at=entry.end_week_at,
                total_distance=entry.distance,
                total_duration=entry.duration)
        else:
            yield report.update_async(
                total_distance=report.total_distance + entry.distance,
                total_duration=report.total_duration + entry.duration)
        raise ndb.Return(report)

    @ndb.tasklet
    def execute_async(self):
        entry = Entry(**self.data)
        _, report = yield (
            entry.put_async(),
            self.update_or_create_report(entry))
        raise ndb.Return(entry, report)

    def execute(self):
        return self.execute_async().get_result()
