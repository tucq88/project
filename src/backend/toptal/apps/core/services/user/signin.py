from django.contrib.auth.hashers import check_password
from google.appengine.ext import ndb

from toptal.libs import utils
from toptal.exceptions import (
    ResourceNotFoundError,
    AuthenticationFailedError)
from toptal.apps.core.models import AccessToken


class UserSigninService(object):

    def __init__(self, username, password):
        self.username = username
        self.password = password

    @ndb.tasklet
    def create_access_token(self, user):
        access_token = yield AccessToken.create_async(
            key=ndb.Key('AccessToken', utils.uuid()),
            user_key=user.key,
            expired_at=utils.now_delta(days=90),
            refresh_token=utils.uuid(),
            refresh_expired_at=utils.now_delta(days=120))
        raise ndb.Return(access_token)

    def check_password(self, user):
        if not check_password(self.password, user.password):
            raise AuthenticationFailedError('password is not correct')

    @ndb.tasklet
    def execute_async(self):
        user = yield ndb.Key('User', self.username).get_async()
        if not user:
            raise ResourceNotFoundError('username does not exist')
        self.check_password(user)
        # TODO: we should have limit number of access token one user could have
        token = yield self.create_access_token(user)
        raise ndb.Return(user, token)

    def execute(self):
        return self.execute_async().get_result()
