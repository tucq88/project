from rest_framework import fields
from rest_framework.serializers import Serializer

from toptal.apps.core.models import User, AccessToken
from toptal.libs.rest_framework.serializers import ModelSerializer


class AccessTokenSerializer(ModelSerializer):

    value = fields.CharField(source='id')

    class Meta:
        model = AccessToken
        fields = (
            'value',
            'refresh_expired_at',
            'refresh_token',
        )


class UserSerializer(ModelSerializer):

    id = fields.CharField(source='safe_id')
    access_token = AccessTokenSerializer()

    class Meta:
        model = User
        fields = (
            'id',
            'name',
            'username',
            'role_type',
            'access_token',
            'created_at',
            'updated_at',
        )


class AccountSignupSerializer(Serializer):

    name = fields.CharField(min_length=3, max_length=100)
    username = fields.CharField(min_length=3, max_length=100)
    password = fields.CharField(min_length=6, max_length=100)


class AccountSigninSerializer(Serializer):

    username = fields.CharField(min_length=3, max_length=100)
    password = fields.CharField(min_length=6, max_length=100)


class UserRefreshTokenSerializer(Serializer):

    refresh_token = fields.CharField()


class UserUpdateSerializer(Serializer):

    name = fields.CharField(min_length=3, max_length=100)
    password = fields.CharField(min_length=6, max_length=100)
