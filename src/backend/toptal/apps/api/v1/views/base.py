from rest_framework.viewsets import ViewSetMixin
from rest_framework.generics import GenericAPIView

from toptal.libs.rest_framework import mixins
from toptal.libs.rest_framework import pagination


class ToptalBaseViewSet(
        mixins.GetObjectMixin,
        mixins.GetSelectedFieldMixin,
        ViewSetMixin,
        GenericAPIView):

    pagination_class = pagination.NDBCursorPagination
