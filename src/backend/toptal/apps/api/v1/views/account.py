from rest_framework.decorators import list_route

from toptal.libs.rest_framework import handlers
from toptal.apps.api.v1.authentications import SignatureAuthentication
from toptal.apps.api.v1.serializers import (
    AccountSignupSerializer,
    AccountSigninSerializer,
    UserSerializer)
from toptal.apps.core.services import (
    UserSignupService,
    UserSigninService)

from base import ToptalBaseViewSet


class SigninMixin(object):

    @list_route(methods=['post'])
    def signin(self, request):
        s = AccountSigninSerializer(data=request.data)
        if s.is_valid():
            user, token = UserSigninService(
                s.validated_data['username'],
                s.validated_data['password']).execute()
            user.access_token = token
            s = UserSerializer(
                user, context={'request': request, 'user': user})
            s.selected_fields = self.get_selected_fields(request)
            return handlers.response_success(s.data)
        return handlers.response_validation_error(s.errors)


class SignupMixin(object):

    @list_route(methods=['post'])
    def signup(self, request):
        s = AccountSignupSerializer(data=request.data)
        if s.is_valid():
            user, token = UserSignupService(
                s.validated_data['username'],
                s.validated_data['name'],
                s.validated_data['password']).execute()
            user.access_token = token
            s = UserSerializer(
                user, context={'request': request, 'user': user})
            s.selected_fields = self.get_selected_fields(request)
            return handlers.response_success(s.data)
        return handlers.response_validation_error(s.errors)


class AccountViewSet(
        SigninMixin,
        SignupMixin,
        ToptalBaseViewSet):

    authentication_classes = [SignatureAuthentication]
