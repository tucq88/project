from rest_framework.authentication import BaseAuthentication

from toptal.apps.core.models import User, AccessToken
from toptal.exceptions import AuthenticationFailedError


class BaseAccessTokenAuthentication(BaseAuthentication):

    ROLE_TYPES = []

    def authenticate(self, request):
        access_token = request.query_params.get('access_token')
        if access_token:
            token = AccessToken.get_by_id(access_token)
            if token and token.is_valid():
                user = token.user_key.get()
                if user.role_type not in self.ROLE_TYPES:
                    raise AuthenticationFailedError(
                        'User does not have permission to call this API')
                return (user, token)
        raise AuthenticationFailedError('Invalid access token')


class AccessTokenUserManagerAuthentication(BaseAccessTokenAuthentication):

    ROLE_TYPES = [
        User.USER_ROLE_TYPE_USER_MANAGER,
        User.USER_ROLE_TYPE_ADMIN
    ]


class AccessTokenAdminAuthentication(BaseAccessTokenAuthentication):

    ROLE_TYPES = [
        User.USER_ROLE_TYPE_USER_MANAGER,
        User.USER_ROLE_TYPE_ADMIN
    ]
