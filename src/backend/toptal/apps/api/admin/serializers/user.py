from rest_framework import fields

from toptal.apps.core.models import User
from toptal.libs.rest_framework.serializers import ModelSerializer


class UserSerializer(ModelSerializer):

    id = fields.CharField(source='safe_id')
    status = fields.ChoiceField(choices=User.USER_STATUS_CHOICES)
    role_type = fields.ChoiceField(choices=User.USER_ROLE_TYPE_CHOICES)

    class Meta:
        model = User
        fields = (
            'id',
            'name',
            'status',
            'username',
            'password',
            'role_type',
            'created_at',
            'updated_at',
        )


class UserManagerUpdateSerializer(ModelSerializer):

    name = fields.CharField(min_length=3, max_length=100)
    username = fields.CharField(min_length=3, max_length=100)
    password = fields.CharField(min_length=6, max_length=100)

    class Meta:
        model = User
        fields = (
            'name',
            'username',
            'password',
        )


class UserAdminUpdateSerializer(ModelSerializer):

    name = fields.CharField(min_length=3, max_length=100)
    username = fields.CharField(min_length=3, max_length=100)
    password = fields.CharField(min_length=6, max_length=100)
    role_type = fields.ChoiceField(choices=User.USER_ROLE_TYPE_CHOICES)
    status = fields.ChoiceField(choices=User.USER_STATUS_CHOICES)

    class Meta:
        model = User
        fields = (
            'name',
            'username',
            'password',
            'role_type',
            'status',
        )
