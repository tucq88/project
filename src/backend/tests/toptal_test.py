import unittest

from rest_framework.test import APIClient
from rest_framework.settings import api_settings
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory

from google.appengine.ext import testbed
from google.appengine.ext import ndb
from google.appengine.datastore import datastore_stub_util

from toptal.libs import utils
from toptal.apps.core.models import User, AccessToken


class RequestFactory(APIRequestFactory):

    def request(self, **kwargs):
        request = super(RequestFactory, self).request(**kwargs)
        parser_classes = [
            parser() for parser in api_settings.DEFAULT_PARSER_CLASSES]
        request = Request(request, parsers=parser_classes)
        return request


class BaseToptalTest(unittest.TestCase):

    def setUpGAE(self):
        self.testbed = testbed.Testbed()
        self.testbed.setup_env(current_version_id='testbed.version')
        self.testbed.activate()
        self.policy = datastore_stub_util.PseudoRandomHRConsistencyPolicy(
            probability=1)
        self.testbed.init_datastore_v3_stub(consistency_policy=self.policy)
        self.testbed.init_taskqueue_stub(root_path='src')
        self.taskqueue_stub = self.testbed.get_stub(
            testbed.TASKQUEUE_SERVICE_NAME)
        self.testbed.init_memcache_stub()
        self.testbed.init_taskqueue_stub(root_path='src')
        self.taskqueue_stub = self.testbed.get_stub(
            testbed.TASKQUEUE_SERVICE_NAME)

    def setUp(self):
        self.setUpGAE()
        if hasattr(self, 'setUpModel'):
            self.setUpModel()
        if hasattr(self, 'setUpTest'):
            self.setUpTest()

    def tearDown(self):
        self.testbed.deactivate()


class BaseToptalViewSetTest(BaseToptalTest):

    VIEW_CLASS = None

    @classmethod
    def setUpClass(cls):
        cls.OLD_AUTH_CLASS = cls.VIEW_CLASS.authentication_classes
        cls.VIEW_CLASS.authentication_classes = []

    def setUpTest(self):
        self.client = APIClient()

    @classmethod
    def tearDownClass(cls):
        cls.VIEW_CLASS.authentication_classes = cls.OLD_AUTH_CLASS


class ForceAuthenticateUserTest(BaseToptalViewSetTest):

    def setUpModel(self):
        self.user = User.create(key=ndb.Key('User', 'test'), name='test')
        self.token = AccessToken.create(
            key=ndb.Key('AccessToken', utils.uuid()),
            user_key=self.user.key,
            expired_at=utils.now_delta(days=90),
            refresh_token=utils.uuid(),
            refresh_expired_at=utils.now_delta(days=120))

    def setUpTest(self):
        self.client = APIClient()
        self.client.force_authenticate(self.user, self.token)
