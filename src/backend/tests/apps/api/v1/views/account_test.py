from toptal.apps.core.services import UserCreateService
from toptal.apps.api.v1.views import AccountViewSet

from tests.toptal_test import BaseToptalViewSetTest


class TestAccountViewSet(BaseToptalViewSetTest):

    VIEW_CLASS = AccountViewSet

    def test_signin(self):
        UserCreateService(
            name='hello', username='aaa', password='password').execute()
        res = self.client.post('/v1/accounts/signin', data={
            'username': 'aaa',
            'password': 'password'
        })
        self.assertEquals(200, res.status_code)

    def test_signup(self):
        res = self.client.post('/v1/accounts/signup', data={
            'name': 'hello',
            'username': 'aaa',
            'password': 'password'
        })
        self.assertEquals(200, res.status_code)
