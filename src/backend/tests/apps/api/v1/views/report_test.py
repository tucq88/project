import json

from toptal.libs import utils
from toptal.apps.api.v1.views import ReportViewSet
from toptal.apps.core.services import EntryCreateService

from tests.toptal_test import ForceAuthenticateUserTest


class TestEntryViewSet(ForceAuthenticateUserTest):

    VIEW_CLASS = ReportViewSet

    def setUpModel(self):
        super(TestEntryViewSet, self).setUpModel()
        for i in xrange(5):
            EntryCreateService(
                self.user, user_key=self.user.key, distance=10,
                start_at=utils.now_delta(days=i * 7),
                end_at=utils.now_delta(days=i * 7, hours=1)).execute()

    def test_list_entry(self):
        res = self.client.get('/v1/reports')
        self.assertEquals(200, res.status_code)
        data = json.loads(res.content)['data']
        self.assertEquals(5, len(data))

    def test_list_entry_with_filter(self):
        res = self.client.get('/v1/reports?start=%s' % (
            utils.now_delta(days=100).date().strftime('%Y-%m-%d')))
        self.assertEquals(200, res.status_code)
        data = json.loads(res.content)['data']
        self.assertEquals(0, len(data))
