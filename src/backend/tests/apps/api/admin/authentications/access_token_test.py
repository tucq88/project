import nose
from google.appengine.ext import ndb

from toptal.libs import utils
from toptal.apps.core.models import User, AccessToken
from toptal.apps.api.admin.authentications import BaseAccessTokenAuthentication
from toptal.exceptions import AuthenticationFailedError

from tests.toptal_test import BaseToptalTest, RequestFactory


class TestAccessTokenAuthentication(BaseToptalTest):

    @classmethod
    def setUpClass(self):
        self.authenticator = BaseAccessTokenAuthentication()

    @nose.tools.raises(AuthenticationFailedError)
    def test_none_access_token(self):
        request = RequestFactory().get('/')
        self.authenticator.authenticate(request)

    @nose.tools.raises(AuthenticationFailedError)
    def test_not_exist_access_token(self):
        request = RequestFactory().get('/', {
            'access_token': '9b398e8c923b46478f78d65063dfbf62',
        })
        self.authenticator.authenticate(request)

    @nose.tools.raises(AuthenticationFailedError)
    def test_invalid_access_token(self):
        user = User.create(key=ndb.Key('User', 'test'), name='test')
        token = AccessToken.create(
            user_key=user.key, expired_at=utils.now_delta(hours=-1))
        request = RequestFactory().get('/', {
            'access_token': token.id,
        })
        self.authenticator.authenticate(request)

    @nose.tools.raises(AuthenticationFailedError)
    def test_invalid_with_access_token_of_normal_user(self):
        user = User.create(key=ndb.Key('User', 'test'), name='test')
        token = AccessToken.create(
            key=ndb.Key('AccessToken', utils.uuid()),
            user_key=user.key, expired_at=utils.now_delta(hours=1))
        request = RequestFactory().get('/', {
            'access_token': token.id,
        })
        self.authenticator.authenticate(request)
