import json

from toptal.apps.api.admin.views import UserViewSet
from toptal.apps.core.models import User
from toptal.apps.core.services import UserCreateService

from tests.toptal_test import ForceAuthenticateUserTest


class TestAdminUserViewSet(ForceAuthenticateUserTest):

    VIEW_CLASS = UserViewSet

    def test_list_user(self):
        for i in xrange(5):
            UserCreateService(
                name='name %s' % i,
                username='username-%s' % i,
                password='test-%s' % i).execute()

        res = self.client.get('/admin-api/users')
        self.assertEquals(200, res.status_code)
        data = json.loads(res.content)['data']
        self.assertEquals(6, len(data))

    def test_list_user_with_filter(self):
        for i in xrange(5):
            UserCreateService(
                name='name %s' % i,
                username='username-%s' % i,
                password='test-%s' % i).execute()

        res = self.client.get('/admin-api/users?username=username-1')
        self.assertEquals(200, res.status_code)
        data = json.loads(res.content)['data']
        self.assertEquals(1, len(data))

    def test_create_user(self):
        res = self.client.post('/admin-api/users', data={
            'username': 'test1',
            'name': 'test 1',
            'password': 'test-1'
        })
        self.assertEquals(200, res.status_code)
        self.assertEquals(2, User.query().count())

    def test_update_user(self):
        self.user.role_type = User.USER_ROLE_TYPE_ADMIN
        user = UserCreateService(
            name='name 1', username='username-1',
            password='test-1').execute()
        res = self.client.put('/admin-api/users/%s' % user.safe_id, data={
            'name': 'test 2'
        })
        self.assertEquals(200, res.status_code)
        self.assertEquals('test 2', user.key.get().name)

    def test_delete_user(self):
        self.user.role_type = User.USER_ROLE_TYPE_ADMIN
        user = UserCreateService(
            name='name 1', username='username-1',
            password='test-1').execute()
        res = self.client.delete('/admin-api/users/%s' % user.safe_id)
        self.assertEquals(200, res.status_code)
        self.assertEquals(User.USER_STATUS_DELETED, user.key.get().status)
